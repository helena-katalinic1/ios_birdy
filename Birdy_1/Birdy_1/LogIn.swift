//
//  LogIn.swift
//  Birdy_1
//
//  Created by student on 19.12.2023..
//

import SwiftUI



struct LogIn: View {
    
    @Binding var username:String
    @Binding var isPresented:Bool//shared
    
    
    var body: some View {
      
        VStack{
            
            Image( "Image").resizable().frame(width:200 , height: 200).clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                .padding()
            Text("Birdy").foregroundStyle(.pink).font(.largeTitle)
                .padding()
            
            TextField("Username", text:$username).padding(.leading, 100).padding(.trailing, 100).padding(.bottom,10).multilineTextAlignment(.center).textFieldStyle(.roundedBorder)
                
                }
        
        Button(action: {isPresented=false}){
            
            Text("Log in ").foregroundColor(.black)
        }.frame(width:75, height:  80)
        .background(Color.red.opacity(0.5))
        .disabled(username.isEmpty)
        
        
        
        
    }
}

#Preview {
    LogIn(username:Binding.constant(""), isPresented:Binding.constant(true))
}
