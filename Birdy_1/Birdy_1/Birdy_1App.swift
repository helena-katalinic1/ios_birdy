//
//  Birdy_1App.swift
//  Birdy_1
//
//  Created by student on 13.12.2023..
//

import SwiftUI

@main
struct Birdy_1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
