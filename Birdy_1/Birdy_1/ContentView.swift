//
//  ContentView.swift
//  Birdy_1
//
//  Created by student on 13.12.2023..
//

import SwiftUI

struct Tweet: Identifiable{
    var id=UUID()  //unique id every time
    let username:String
    var  content:String
    let date:Date=Date()
    var isFavourite:Bool
    
}

///

struct ContentView: View {
   
    
    @State var tweets:[Tweet]=[
        Tweet(username: "henry", content: "witcher",  isFavourite: false),
        Tweet(username: "helki", content: "sasd",  isFavourite: false),
        Tweet(username: "base", content: "witcgfher",  isFavourite: false)
      
    ]
    
    @State  var content: String = ""
    @State var isPresent:Bool=false
    @State var username:String=""
    
    var body: some View {
        
       
                
        VStack {
            HStack{
                
                Image(systemName: "bird").padding()
                    .imageScale(.large)
                    .foregroundStyle(.tint)
                Text("Birdy").foregroundStyle(.pink).font(.largeTitle)
                
                
                Spacer()
                
                if username.isEmpty{
                    Button(action:{isPresent=true}){Text("Log in")}
                }
                else {
                    Button(action:{username=""}){Text("Log out")}
                }
            }
            
            
            List($tweets){
                
                tweet in TweetRow(tweet:tweet)
            }.listStyle(.plain)
            
            //  TweetRow(tweet:$tweet)
            Spacer()
            if !username.isEmpty{
                
                HStack{
                    TextField("Content", text:$content)
                    
                    Button(action:{
                        
                        tweets.append(Tweet(
                            username: username,
                            content: content,
                            
                            isFavourite: false
                            
                            
                        ));content=""
                    })
                    {
                        Text("New Tweet")
                        
                    }.disabled(content.isEmpty )
                    
                }
            }}
            .padding()
            .sheet(isPresented: $isPresent) {
                LogIn(username: $username, isPresented:$isPresent)
            }
        
        }}

#Preview {
    ContentView()
}
