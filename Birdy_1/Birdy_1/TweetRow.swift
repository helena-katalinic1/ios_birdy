//
//  TweetRow.swift
//  Birdy_1
//
//  Created by student on 19.12.2023..
//

import SwiftUI

struct TweetRow: View {
    
    @Binding var tweet:Tweet
    
    var body: some View {
        HStack{
            Image( "Image").resizable().frame(width:100 , height: 100).clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            
            VStack{
                Text(tweet.username)
                Text(tweet.content)
                Text(tweet.date, style:.relative)
                    .font(.caption)
                    .foregroundColor(.gray)
                
            }
            Spacer().padding(.leading)
            Button(action:{tweet.isFavourite.toggle()})
            {
                if tweet.isFavourite{
                    Image(systemName: "heart.fill").foregroundColor(.red)
                }
                else {Image(systemName: "heart").foregroundColor(.red)}
            }
            
        }
    }
}

#Preview {
    TweetRow(tweet:Binding.constant(Tweet(username: "Helki", content:"bruh",isFavourite: true)))
}
